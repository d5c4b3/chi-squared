// This code was not written to my usual standard. It's one large file written
// exactly for this page/purpose with only jquery and bootstrap. It's not meant
// to be re-used.

// CONSTANTS
const SIG_NONE = 0;
const SIG_LOW = 1;
const SIG_MED = 2;
const SIG_HIGH = 3;
const SIG_V_HIGH = 4;

// Indexed by SIG level
// ex: let needed = dice_rolls * ROLLS_REQ_MULT[SIG_LOW];
const ROLLS_REQ_MULT = [0, 5, 15, 25, 50];

// Indexed by SIG level
// Rather than comparing the test-value, df, and selected signifiance to a table to
// determine if we should reject null hypothesis we calculate the probability of
// the test-value appearing given a df and compare to what we would expect given 
// the significance level. 
const PROBABILITY_LESS_CRIT_VALUE = [Infinity, 0.95, 0.975, 0.99, 0.999];

// To explain in more detail. In order to determine if we should reject the null
// hypothesis we have two processes. 
// In both processes we must calculate test-statistic (Z), 
// calculate degrees of freedom (df), and select a significance level. 
//
// In the usual proccess we would find a row in the critical-value table corresponding
// to the degrees of freedom. We would then select the column of the table 
// based on our significance level (ex: 95%). If the value in the table is less than
// Z we reject otherwise we fail to reject
//
// In the process I'm using, we calculate the probability that our Z is less than
// the critical value using the Chi-Square distribution function and compare that
// to our selected significance level. If the calculated is higher than selected
// we reject otherwise we fail to reject.


// Following code copied from http://www.math.ucla.edu/~tom/distributions/chisq.html
class Chi {

	static logGamma(Z) {
		var S=1+76.18009173/Z-86.50532033/(Z+1)+24.01409822/(Z+2)-1.231739516/(Z+3)+.00120858003/(Z+4)-.00000536382/(Z+5);
		var LG= (Z-.5)*Math.log(Z+4.5)-(Z+4.5)+Math.log(S*2.50662827465);
		return LG;
	}

	static gcf(X,A) {        // Good for X>A+1
		var A0=0;
		var B0=1;
		var A1=1;
		var B1=X;
		var AOLD=0;
		var N=0;
		while (Math.abs((A1-AOLD)/A1)>.00001) {
			AOLD=A1;
			N=N+1;
			A0=A1+(N-A)*A0;
			B0=B1+(N-A)*B0;
			A1=X*A0+N*A1;
			B1=X*B0+N*B1;
			A0=A0/B1;
			B0=B0/B1;
			A1=A1/B1;
			B1=1;
		}
		var Prob=Math.exp(A*Math.log(X)-X-this.logGamma(A))*A1;
		return 1-Prob;
	}

	static gser(X,A) {        // Good for X<A+1.
		var T9=1/A;
		var G=T9;
		var I=1;
		while (T9>G*.00001) {
			T9=T9*X/(A+I);
			G=G+T9;
			I=I+1;
		}
		G=G*Math.exp(A*Math.log(X)-X-this.logGamma(A));
		return G;
	}

	static gammacdf(x,a) {
		var GI;
		if (x<=0) {
			GI=0
		} else if (x<a+1) {
			GI=this.gser(x,a)
		} else {
			GI=this.gcf(x,a)
		}
		return GI;
	}

	/**
	 * Given a chi^2 value (z-score) and a degrees of freedom 
	 * calculates the right-tailed probably that chi^2 is less than the
	 * critical value
	 */
	static cdf(z, df) {
		if (df <= 0) {
			throw new Error("degrees of freedom must be positive");
		}

		return this.gammacdf(z/2, df/2);
	}
};


/**
 * Checks if the Single Roll input is valid and sets error messages 
 * accordingly
 */
function validate_roll() {
	let $single = $('#roll-input');
	let $all = $('#rolls-input');
	let $dice = $('#dice-select');
	let $single_error = $('#roll-input-error');
	$single_error.empty(); // reset error message

	let roll = $single.val().trim();

	if ($single.val().trim() === "") {
		return false;
	}

	// In the case that the user is rolling a d10 
	// and they type a 0 automatically convert that to 10
	if ($dice.val() === '10' && roll === '0') {
		roll = '10';
	}

	let num = Number.parseInt(roll);
	if (!Number.isInteger(num)) {
		$single.get(0).setCustomValidity("Roll must be a number");
		$single_error.text("Roll must be a number");
		return false;
	}

	let max = Number.parseInt($dice.val());
	if (num < 1 || num > max) {
		$single.get(0).setCustomValidity("Roll must be between 1 and "+max);
		$single_error.text("Roll must be between 1 and "+max);
		return false;
	}

	return true;
}

/**
 * The Chart is responsive and will adjust to the viewport. This is to be used
 * in a 'resize' listener. Calling it will check container width and update
 * the canvas and redraw the Chart.
 */
function resize_chart_canvas() {
	let canvas = document.getElementById('freq-chart');
	let ctx = canvas.getContext('2d');
	let width = $('#freq-chart-wrapper').width();
	const aspect_ratio = 0.6;
	canvas.width = width;
	canvas.height = width * aspect_ratio;

	for (let id in Chart.instances) {
		Chart.instances[id].update();
	}
}

/**
 * Return an array containing the number of times a dice face was rolled. Dice
 * faces are expected to be 1 indexed (except d10), but the returned array is 0 indexed. 
 *
 * For a d10 a '0' roll is converted to 10.
 *
 * Ex. for d4, [1, 1, 1, 2, 2, 2, 2, 2, 3] returns
 * [3, 5, 1, 0]
 *
 * Ex. for d10, [8, 3, 8, 9, 0, 0, 0, 4] returns
 * [0, 0, 1, 1, 0, 0, 0, 8, 1, 3]
 */
// TODO find a way to cache this. It's currenty getting called 3x per update
function roll_frequency(rolls, dice_max) {
	let freq = new Array(dice_max).fill(0);

	for (let i = 0; i < rolls.length; i++) {
		let roll = rolls[i];

		// Handle d10
		if (dice_max === 10 && roll === 0) {
			roll = 10;
		}

		// Subtract 1 from roll since arrays are 0-indexed
		freq[roll-1]++;
	}

	return freq;
}

/**
 * Calculates the test-statistic for a Chi-square test.
 */
function chi_squared(rolls, dice_max) {
	let expected = rolls.length/dice_max;
	let freq = roll_frequency(rolls, dice_max);

	let sum = 0;
	for (let i = 0; i < freq.length; i++) {
		let diff = freq[i] - expected;
		sum += (diff*diff)/expected;
	}

	return sum;
}

/**
 * Updates the data object used by a chart. The data param is expected to
 * be an object and it will be modified by this function.
 */
function update_chart_data(data, dice_max, rolls) {
	// If empty or dice_max is not the same as previous reinitialize data
	if ($.isEmptyObject(data) || data.labels.length != dice_max) {
		// Set label
		if (dice_max === 10) {
			data.labels = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0/10'];
		} else {
			// array from 1 to dice_max as strings
			data.labels = [...Array(dice_max).keys()].map(x => ""+(x+1));
		}

		// Initialize datasets
		data.datasets = [];
		data.datasets[0] = {
			label: "Expected",
			backgroundColor: "grey",
			data: new Array(dice_max).fill(0)
		};
		data.datasets[1] = {
			label: "Observed",
			backgroundColor: "blue",
			data: new Array(dice_max).fill(0)
		};
	}

	// Update datasets
	data.datasets[0].data.fill(rolls.length/dice_max);
	data.datasets[1].data = roll_frequency(rolls, dice_max);
}

/**
 * Calculates the number of rolls needed to be at a particular significance level.
 * Values outside of the normal range for sig_level are accepted.
 */
function rolls_needed(dice_max, sig_level) {
	if (sig_level < 0) return 0;
	if (sig_level > SIG_V_HIGH) return dice_max * ROLLS_REQ_MULT[SIG_V_HIGH];

	return dice_max * ROLLS_REQ_MULT[sig_level];
}

/**
 * Determines which significance level we should use based on the number of 
 * rolls the user has done. Lower roll counts get a lower significance level
 * (less likely hood of determining bias).
 */
function significance_level(num_rolls, dice_max) {
	if (num_rolls < rolls_needed(dice_max, SIG_LOW)) {
		return SIG_NONE; // No significance
	} else if (num_rolls < rolls_needed(dice_max, SIG_MED)) {
		return SIG_LOW; // low significance
	} else if (num_rolls < rolls_needed(dice_max, SIG_HIGH)) {
		return SIG_MED; // med significance
	} else if (num_rolls < rolls_needed(dice_max, SIG_V_HIGH)) {
		return SIG_HIGH; // high significance
	} else {
		return SIG_V_HIGH; // very high significance
	}
}

/**
 * Should we reject the null hypothesis of "Dice roll frequency meets fair dice expectations"
 *
 * This shouldn't update the UI, but it does because it was the easiest way 
 * to do it at the time. Don't do this.
 */
function should_reject_hypothesis(rolls, dice_max) {
	let sig_lvl = significance_level(rolls.length, dice_max);
	let test_statistic = chi_squared(rolls, dice_max);
	let cdf_probability = Chi.cdf(test_statistic, dice_max-1);


	// TODO: move this somewhere that makes more sense
	if (sig_lvl === SIG_NONE) {
		$('#stats').hide();
	} else {
		let trailing_zeros = new RegExp('(?:(\\.\\d*?[1-9]+)|\\.)0*$');
		$('#stat-significance').text(PROBABILITY_LESS_CRIT_VALUE[sig_lvl]);
		$('#stat-chi-square').text(test_statistic.toFixed(4).replace(trailing_zeros, '$1'));
		$('#stat-cdf').text(cdf_probability.toFixed(4).replace(trailing_zeros, '$1'));
		$('#stats').show();
	}

	return cdf_probability > PROBABILITY_LESS_CRIT_VALUE[sig_lvl];
}

/**
 * Updates all roll count and rolls needed ui elements.
 */
function update_roll_count(num_rolls, rolls_needed, sig_level) {
	$('#curr-roll-count,#form-roll-count').text(num_rolls);
	$('#rolls-needed').text(rolls_needed);
	switch (sig_level) {
		default:
		case SIG_NONE:
			$('#curr-roll-count,#form-roll-count')
				.removeClass('badge-warning badge-success badge-primary badge-info')
				.addClass('badge-danger');
			$('#rolls-needed')
				.removeClass('badge-danger badge-success badge-primary badge-info')
				.addClass('badge-warning');
			$('#needed-sig-med,#needed-sig-high,#needed-sig-vhigh').hide();
			$('#needed-sig-low').show();
			$('#at-highest-sig').hide();
			$('#rolls-needed-wrapper').show();
			break;
		case SIG_LOW:
			$('#curr-roll-count,#form-roll-count')
				.removeClass('badge-danger badge-success badge-primary badge-info')
				.addClass('badge-warning');
			$('#rolls-needed')
				.removeClass('badge-danger badge-success badge-primary badge-warning')
				.addClass('badge-info');
			$('#needed-sig-low,#needed-sig-high,#needed-sig-vhigh').hide();
			$('#needed-sig-med').show();
			$('#at-highest-sig').hide();
			$('#rolls-needed-wrapper').show();
			break;
		case SIG_MED:
			$('#curr-roll-count,#form-roll-count')
				.removeClass('badge-danger badge-success badge-primary badge-warning')
				.addClass('badge-info');
			$('#rolls-needed')
				.removeClass('badge-danger badge-success badge-info badge-warning')
				.addClass('badge-primary');
			$('#needed-sig-med,#needed-sig-low,#needed-sig-vhigh').hide();
			$('#needed-sig-high').show();
			$('#at-highest-sig').hide();
			$('#rolls-needed-wrapper').show();
			break;
		case SIG_HIGH:
			$('#curr-roll-count,#form-roll-count')
				.removeClass('badge-danger badge-success badge-info badge-warning')
				.addClass('badge-primary');
			$('#rolls-needed')
				.removeClass('badge-danger badge-warning badge-primary badge-info')
				.addClass('badge-success');
			$('#needed-sig-med,#needed-sig-low,#needed-sig-high').hide();
			$('#needed-sig-vhigh').show();
			$('#at-highest-sig').hide();
			$('#rolls-needed-wrapper').show();
			break;
		case SIG_V_HIGH:
			$('#curr-roll-count,#form-roll-count')
				.removeClass('badge-danger badge-primary badge-info badge-warning')
				.addClass('badge-success');
			$('#rolls-needed-wrapper').hide();
			$('#at-highest-sig').show();
			break;
	}
}

/**
 * Updates ui elements such as roll counts, chart data, and conclusion.
 */
function update_ui(rolls, dice_max, freq_chart) {
	let sig_level = significance_level(rolls.length, dice_max);
	let num_needed = rolls_needed(dice_max, sig_level+1);
	let reject = should_reject_hypothesis(rolls, dice_max);

	// Update roll counts
	update_roll_count(rolls.length, num_needed, sig_level);

	// Update chart
	update_chart_data(freq_chart.data, dice_max, rolls);
	freq_chart.update();

	// Update conclusion
	if (sig_level === SIG_NONE) {
		$('#ans-reject,#ans-fail-to-reject').hide();
		$('#ans-na').show();
	} else if (reject) {
		$('#ans-na,#ans-fail-to-reject').hide();
		$('#ans-reject').show();
	} else {
		$('#ans-na,#ans-reject').hide();
		$('#ans-fail-to-reject').show();
	}

	// Update permalink
	set_permalink(rolls, dice_max);
}

/**
 * Pulls die size and rolls from URL
 * @see set_permalink for encoding details
 */
function initialize_from_url() {
	let params = new URLSearchParams(window.location.search);

	// If we are missing a param exit early
	if (!(params.has('d') && params.has('r'))) {
		return;
	}

	//TODO: do some validation on this rather than blindly trust
	let die_max = Number.parseInt(params.get('d'));
	if (!Number.isInteger(die_max)) {
		console.error('Error converting '+params.get('d')+' to integer');
		return;
	}

	let freq = atob(params.get('r')).split(',');
	let rolls = [];

	if (die_max != freq.length) {
		console.error('Pre-process freq decode sanity check failed');
		return;
	}

	for(let i = 0; i < freq.length; i++) {
		let num = Number.parseInt(freq[i]);
		if (!Number.isInteger(num)) {
			console.error('Error converting '+freq[i]+' to integer in freq process');
			return;
		}

		let n = i+1;
		for (let j = 0; j < num; j++) {
			rolls.push(n);
		}
	}

	// Set calculated values
	$('#dice-select').val(die_max);
	$('#rolls-input').val(rolls.join(','));
}

/**
 * URL encodes rolls and die_max and sets permalink href
 *
 * die_max is added directly as param 'd'
 * rolls are param 'r' encoded as base64 encoded roll_frequency
 */
function set_permalink(rolls, die_max) {
	let str = "?d="+die_max+"&r=";

	// freq is much smaller than actually using the rolls
	let freq = roll_frequency(rolls, die_max);

	str += btoa(freq.join(','));

	$('#permalink').attr('href', str).show();
}

/**
 * Parse input values then trigger update
 * this is not optimized at all and is probably going to break if
 * users input stupid stuff. 
 */
function handle_data_change(freq_chart) {
	let $single = $('#roll-input');
	let $all = $('#rolls-input');
	let $dice = $('#dice-select');
	let $curr_count = $('#curr-roll-count');
	let $needed_count = $('#rolls-needed');

	// Convert roll list into array of integers
	let rolls = $all.val().split(',') // split csv into array
		.reduce(function (filtered, value) {
			let num = Number.parseInt(value.trim());
			if (Number.isInteger(num)) {
				filtered.push(num);
			}
			return filtered;
		}, []);

	// Handle single roll input. Check if valid and add to rolls
	if (validate_roll()) {
		// This could be handled much more cleanly
		rolls.push(Number.parseInt($single.val().trim()));
		$all.val(rolls.join(",")); // update all rolls
	}

	// Clear input
	$single.val('');


	// Update everything
	let dice_max = Number.parseInt($dice.val());
	update_ui(rolls, dice_max, freq_chart);
}

/**** Initialize variables and ui elements then set event listeners ****/
$(document).ready(function() {
	let $single = $('#roll-input');
	let $all = $('#rolls-input');
	let $dice = $('#dice-select');
	let $curr_count = $('#curr-roll-count');
	let $needed_count = $('#rolls-needed');

	// if url data is present update input values
	initialize_from_url();

	let dice_max = Number.parseInt($dice.val());

	// Setup Chart
	resize_chart_canvas();

	let canvas = document.getElementById('freq-chart');
	let ctx = canvas.getContext('2d');

	let data = {};

	let freq_chart = new Chart(ctx, {
		type: 'bar',
		data: data,
		options: {
			scales: {
				yAxes: [{
					ticks: {
						min: 0,
					}
				}]
			},
			responsive: true,
			maintainAspectRatio: true
		}
	});

	// Call this once manually to initialize everything
	handle_data_change(freq_chart);


	// Set up event listeners
	$('#dice-rolls-form').submit(function(e) {
		// Using a form for semantics, but don't actually submit the form
		// just update the page
		e.preventDefault();

		// Actually do all the calculations and ui changes
		handle_data_change(freq_chart);
	});

	$single.on('input', function(e) {
		e.target.setCustomValidity('');
	});

	$(window).on('resize', function(e) {
		resize_chart_canvas();
	});
});
